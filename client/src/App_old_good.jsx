import React, { Component } from 'react';

import Voting from "./contracts/Voting.json";

import getWeb3 from "./getWeb3";

import VoterRegistration from './components/VoterRegistration';
import Proposal from './components/Proposal';
import VotingSession from './components/VotingSession';
import Results from './components/Results';
class App extends Component {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      web3: null,
      accounts: null,
      contract: null,
      userAddress: null,
     // proposals: [],
      isOwner: false,
      workflowStatus: null,
      winningProposalId: null,
      //registeredVoter: false,
      //voterHasVoted: false,
      //registerAccount: "",
      registeredVoters: [],
      registeredProposals: [],
      contractOwner: "",
      newProposalDescription: '',
      registeredVotes:  [],
      hasVoted: [],
      proposalId: -1,
    };
  }

  initMount = async () => {
    try {
      const web3 = await getWeb3();
      const accounts = await web3.eth.getAccounts();
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Voting.networks[networkId];
      
      const instance = new web3.eth.Contract(
        Voting.abi,
        deployedNetwork?.address
      );

      instance.events.WorkflowStatusChange()
        .once('data', (event) => {
          this.loadContractData();
      });

      instance.events.Voted()
        .once('data', (event) => {
          this.loadContractData();
      })
      
      const contractOwner = await instance.methods.owner().call();
      const isOwner = contractOwner.toLowerCase() === accounts[0].toLowerCase();

      this.setState({ web3, accounts, contract: instance, userAddress: accounts[0], contractOwner: contractOwner, isOwner: isOwner}, () => {
        this.loadContractData();
      });
    } catch(error) {

    }
  }

  componentDidMount = async () => {
    try {
      this.initMount();

      window.ethereum.on("accountsChanged", this.handleAccountChange);
    } catch(error) {
      alert(`Failed to load web3, accounts, or contract. Check console for details.`);
    }
  }

  componentDidUpdate = (prevState) => {
    if (prevState.userAddress !== this.state.userAddress) {
      this.loadContractData();
    }
  }

  handleAccountChange = (accounts) => {
    if (accounts.length > 0) {
      const newAccount = accounts[0];
      const isOwner = this.state.contractOwner.toLowerCase() === accounts[0].toLowerCase();
      this.setState({ userAddress: newAccount, isOwner: isOwner }, () => {
        this.loadContractData();
      });
    }
  }

  loadContractData =  async ()  => {
    const { contract, accounts } = this.state;

    if (contract) {
     //const proposals = await contract.methods.getProposals().call();
      const registeredVoter = await contract.methods.voters(accounts[0]).call();
      const workflowStatus = await contract.methods.getWorkflowStatus().call();
      const voterHasVoted = registeredVoter.hasVoted;

      this.setState({  workflowStatus: workflowStatus, registeredVoter, voterHasVoted });
    }
  }

  handleRegisterVoter = async (account) => {
    const { contract, web3, registeredVoters, isOwner } = this.state;

    if (!web3.utils.isAddress(account)) {
      alert("Adresse Ethereum invalide");
      return;
    }

    try {
      if (!isOwner) {
        alert("Seul le propriétaire du contrat peut enregistrer des électeurs.");
        return;
      }

      await contract.methods.registerVoter(account).send({ from: this.state.userAddress });
      this.loadContractData();
      this.setState({ registeredVoters: [...registeredVoters, account] });
    } catch (error) {
      console.error("Erreur lors de l'enregistrement de l'électeur :", error);
    }
  }

  startProposalsRegistration = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut commencer l'enregistrement des propositions.");
      return;
    }

    try {
      await contract.methods.startProposalsRegistration().send({ from: this.state.userAddress });
      this.loadContractData();
      this.setState({ workflowStatus: 'ProposalsRegistrationStarted'})
    } catch (error) {
      console.error("Erreur lors du démarrage de l'enregistrement des propositions :", error);
    }
  }

  endProposalsRegistration = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut terminer l'enregistrement des propositions.");
      return;
    }

    try {
      await contract.methods.endProposalsRegistration().send({ from: this.state.userAddress });

      this.loadContractData();
    } catch (error) {
      console.error("Erreur lors de la clôture de l'enregistrement des propositions :", error);
    }
  }

  registerProposal = async (newProposalDescription) => {
    const { contract, userAddress, registeredVoters, registeredProposals } = this.state;

    if (!registeredVoters.map(address => address.toLowerCase()).includes(userAddress.toLowerCase())) {
      alert("Seul les votants inscrits peuvent enregistrer des propositions.");
      return;
    }

    try {
      await contract.methods.registerProposal(newProposalDescription).send({ from: this.state.userAddress });

      this.loadContractData();
      this.setState({ registeredProposals: [...registeredProposals, newProposalDescription] });
    } catch (error) {
      console.error("Erreur lors de l'enregistrement de la proposition :", error);
    }
  }

  startVotingSession = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut commencer la session de vote.");
      return;
    }

    try {
      await contract.methods.startVotingSession().send({ from: this.state.userAddress });
      
      this.loadContractData();
    } catch (error) {
      console.error("Erreur lors du démarrage de la session de vote :", error);
    }
  }

  endVotingSession = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut terminer la session de vote.");
      return;
    }

    try {
      await contract.methods.endVotingSession().send({ from: this.state.userAddress });

      this.loadContractData();
    } catch (error) {
      console.error("Erreur lors de la fermeture de la session de vote :", error);
    }
  }

  registerVote = async (proposalId) => {
    const { contract, userAddress, registeredVoters, registeredVotes, hasVoted } = this.state;

    if (!registeredVoters.map(address => address.toLowerCase()).includes(userAddress.toLowerCase())) {
      alert("Seul les votants inscrits peuvent voter.");
      return;
    }

    try {
      if (hasVoted.map(address => address.toLowerCase()).includes(userAddress.toLowerCase())) {
        alert("Vous avez déjà voté.")
        return;
      }

      const newProposalId = parseInt(proposalId, 10);

      await contract.methods.vote(newProposalId).send({ from: this.state.userAddress });

      this.loadContractData();
      this.setState({ registeredVotes: [...registeredVotes, proposalId], hasVoted: [...hasVoted, userAddress] });
    } catch (error) {
      console.error("Erreur lors de l'enregistrement du vote :", error);
    }
  }

  handleTallyVotes = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut comptabiliser les votes.");
      return;
    }

    try {
      await contract.methods.tallyVotes().send({ from: this.state.userAddress });

      this.loadContractData();
    } catch (error) {
      console.error("Erreur lors de la comptabilisation des votes :", error);
    }
  }

  handleGetWinner = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut afficcher le gagant.");
      return;
    }

    try {
      const winner = await contract.methods.getWinner().call({ from: this.state.userAddress });
      
      this.setState({ winningProposalId: winner}, () => {
        this.loadContractData();
      })
    } catch (error) {
      console.error("Erreur lors de l'obtention du gagnant :", error);
    }
  }

  handleReset = async () => {
    const { contract, isOwner } = this.state;

    if (!isOwner) {
      alert("Seul le propriétaire du contrat peut reset le contrat pour un nouveau vote.");
      return;
    }

    try {
      await contract.methods.resetContract().send({ from: this.state.userAddress });

      this.setState({workflowStatus: -1, registeredProposals: [], winningProposalId: null})
      this.loadContractData();
      
    } catch (error) {
      console.error("Erreur lors de la réinitialisation du vote :", error);
    }
  }

  render() {
    const {
      userAddress,
      workflowStatus,
      winningProposalId,
      registeredProposals,
      registeredVoters,
      registeredVotes,
      contractOwner
    } = this.state;

    return (
      <div className="container">
        <div className='row'>
          <p className='col bg-danger'>Compte propriètaire : {contractOwner}</p>
          <p className='col bg-success'>Compte connecté : {userAddress}</p>
          <p className='col bg-warning'>Statut du workflow : {workflowStatus}</p>
        </div>
        <div className='row'>
          <div className='col bg-secondary'>
            <p>Accounts register :</p>
            <ul className="list-group">
              {registeredVoters.map((voter, index) => (
                <li className="list-group-item" key={index}>{voter}</li>
              ))}
            </ul>
          </div>
          <div className='col bg-secondary'>
            <p>Propals register :</p>
            <ul className="list-group">
                {registeredProposals.map((proposal, index) => (
                  <li className="list-group-item" key={index}>{proposal}</li>
                ))}
            </ul>
          </div>
        </div>

        {workflowStatus === "0" && (
          <VoterRegistration
            onRegisterVoter={this.handleRegisterVoter}
            onStartPropals={this.startProposalsRegistration}
            registeredVoters={registeredVoters}
          />
        )}
          
        {workflowStatus === "1" && (
          <Proposal
            onEndPropals={this.endProposalsRegistration}
            registeredProposals={registeredProposals}
            registerProposal={this.registerProposal}
          />
        )}

        {workflowStatus === "2" && (
          <div>
             <button className='btn btn-warning' onClick={this.startVotingSession}>Commencer l'enregistrement des votes</button>
          </div>
        )}

        {workflowStatus === "3" && (
          <VotingSession
            registeredProposals={registeredProposals}
            registeredVotes={registeredVotes}
            registerVote={this.registerVote}
            onEndSession={this.endVotingSession}
          />
        )}

        {workflowStatus === "4" && (
          <div>
            <button className='btn btn-warning' onClick={this.handleTallyVotes}>Comptabilisation des votes</button>
          </div>
        )}

        {workflowStatus === "5" && winningProposalId === null && (
          <div>
            <button className='btn btn-warning' onClick={this.handleGetWinner}>Obtenir le gagnant</button>
          </div>
        )}

        {winningProposalId !== null && (
          <Results
            winner={registeredProposals[winningProposalId]}
            onReset={this.handleReset}
          />
        )}
      </div>
    );
  }
}

export default App;