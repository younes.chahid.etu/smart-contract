import React, { Component } from 'react'

class Proposal extends Component {
    constructor(props) {
        super(props)
        this.state = {
          newProposalDescription: '',
        }
    }

    handleInputChange = (e) => {
        this.setState({ newProposalDescription: e.target.value })
    }

    handleRegisterProposal = () => {
        const { newProposalDescription } = this.state

        if (newProposalDescription) {
            this.props.registerProposal(newProposalDescription)
            this.setState({registerProposal: ''})
        }
    }

    handleEndProposalsRegistration = () => {
        this.props.onEndPropals()
    }

    render() {
        return (
            <div className='container'>
                <h2>Enregistrement des propositions</h2>
                <div className="row h-100">
                    <div className="col-sm-12 my-auto">
                        <div className="input-group w-50 mx-auto">
                            <input className="form-control"
                                type="text"
                                placeholder="Description de la proposition..."
                                value={this.state.newProposalDescription}
                                onChange={this.handleInputChange}
                            />
                            <button className="btn btn-primary" onClick={this.handleRegisterProposal} >Enregistrer la proposition</button>
                        </div>
                    </div>
                </div>
                <button className='btn btn-warning' onClick={this.handleEndProposalsRegistration}>Terminer l'enregistrement des propositions</button>
            </div>
        )
    }
}

export default Proposal