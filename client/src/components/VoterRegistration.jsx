import React, { Component } from 'react'

class VoterRegistration extends Component {
  constructor(props) {
    super(props)

    this.state = {
      registerAccount: '',
    }
  }

  handleInputChange = (e) => {
    this.setState({ registerAccount: e.target.value })
  }

  handleRegisterVoter = () => {
    const { registerAccount } = this.state

    if (registerAccount) {
      this.props.onRegisterVoter(registerAccount)
      this.setState({ registerAccount: '' })
    }
  }

  handleStartProposalsRegistration = () => {
    this.props.onStartPropals()
  }

  render() {
    return (
      <div className='container'>
        <h2>Enregistrement des électeurs</h2>
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="input-group w-50 mx-auto">
              <input className="form-control"
                type="text"
                placeholder="Adresse du compte..."
                value={this.state.registerAccount}
                onChange={this.handleInputChange}
              />
              <button className="btn btn-primary" onClick={this.handleRegisterVoter}>Enregistrer l'électeur</button>
            </div>
          </div>
        </div>
        <button className='btn btn-warning' onClick={this.handleStartProposalsRegistration}>Démarrer l'enregistrement des propositions</button>
      </div>
    )
  }
}

export default VoterRegistration