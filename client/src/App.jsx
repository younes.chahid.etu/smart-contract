import React, { Component } from 'react'

import Voting from "./contracts/Voting.json"

import getWeb3 from "./getWeb3"

import VoterRegistration from './components/VoterRegistration'
import Proposal from './components/Proposal'
import VotingSession from './components/VotingSession'
import Results from './components/Results'

const INITIAL_STATE = {
    web3: null,
    account: null,
    contract: null,
    userAddress: null,
    proposals: [],
    isOwner: false,
    workflowStatus: "0",
    winningProposalId: null,
    register: false,
    voterHasVoted: false,
    voters: [],
    votes: [],
    contractOwner: null
};

class App extends Component {
    constructor(props) {
        super(props)
        this.state = { ...INITIAL_STATE };
    }

    componentDidMount = async () => {
        try {
          this.initMount()
          window.ethereum.on("accountsChanged", this.handleAccountChange)
        } catch(error) {
          alert(`Failed to load web3, accounts, or contract. Check console for details.`)
        }
    }

    registerEventHandlers = () => {
        const instance = this.state.contract;

        const handleVoterRegistered = async (event) => {
            const voterAddress = event.returnValues.voterAddress;
            const contractStructVoters = await instance.methods.voters(voterAddress).call();

            if (contractStructVoters.isRegistered) {
                const newVoters = new Set([...this.state.voters, voterAddress]);
                this.setState({ voters: [...newVoters], register: true });
            }
        };

        const handleWorkflowStatusChange = async (event) => {
            const workflowStatus = await instance.methods.getWorkflowStatus().call();
            this.setState({ workflowStatus: workflowStatus });
        };

        const handleProposalRegistered = async (event) => {
            const proposalId = event.returnValues.proposalId;
            const proposal = await instance.methods.getProposals().call();

            if (!this.state.proposals.includes(proposal[proposalId])) {
                this.setState({ proposals: [...this.state.proposals, proposal[proposalId]] });
            }
        };

        const handleVoted = async (event) => {
            const proposalId = event.returnValues.proposalId;
            const { votes, voterHasVoted } = this.state;

            if (!voterHasVoted) {
                this.setState({ votes: [...votes, proposalId], voterHasVoted: true });
            }
        };

        instance.events.VoterRegistered().on('data', handleVoterRegistered);
        instance.events.WorkflowStatusChange().on('data', handleWorkflowStatusChange);
        instance.events.ProposalRegistered().on('data', handleProposalRegistered);
        instance.events.Voted().on('data', handleVoted);
    }

    initMount = async () => {
        try {
            const web3 = await getWeb3();
            const accounts = await web3.eth.getAccounts();
            const networkId = await web3.eth.net.getId();
            const deployedNetwork = Voting.networks[networkId];
            const instance = new web3.eth.Contract(Voting.abi, deployedNetwork?.address);
            const contractOwner = await instance.methods.owner().call();
            const isOwner = contractOwner.toLowerCase() === accounts[0].toLowerCase();

            this.setState({
                web3: web3,
                accounts: accounts,
                contract: instance,
                userAddress: accounts[0],
                contractOwner: contractOwner,
                isOwner: isOwner
            }, this.registerEventHandlers);
        } catch (error) {
            console.error("Erreur lors de l'initialisation du state :", error);
        }
    }

    handleAccountChange = async (accounts) => {
        if (accounts.length > 0) {
            const newAccount = accounts[0]
            const contractStructVoters = await this.state.contract.methods.voters(accounts[0]).call();
            const isOwner = this.state.contractOwner.toLowerCase() === accounts[0].toLowerCase()

            this.setState({ userAddress: newAccount, isOwner: isOwner, register: contractStructVoters.isRegistered, voterHasVoted: contractStructVoters.hasVoted  })
        }
    }

    handleRegisterVoter = async (account) => {
        const { contract, web3, isOwner } = this.state
    
        if (!web3.utils.isAddress(account)) {
          alert("Adresse Ethereum invalide")
          return
        }
    
        try {
            const contractStructVoters = await contract.methods.voters(account).call();

            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut enregistrer des électeurs.")
                return
            }         
            if (!contractStructVoters.isRegistered) {
                await contract.methods.registerVoter(account).send({ from: this.state.userAddress })
                this.setState({ account: account })
            } else {
                alert("L'électeur est déjà inscrit.")
            }
        } catch (error) {
            console.error("Erreur lors de l'enregistrement de l'électeur :", error)
        }
      }

      
    startProposalsRegistration = async () => {
        const { contract, isOwner } = this.state

        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut commencer l'enregistrement des propositions.")
                return
            }

            await contract.methods.startProposalsRegistration().send({ from: this.state.userAddress })
        } catch (error) {
            console.error("Erreur lors du démarrage de l'enregistrement des propositions :", error)
        }
    }

    endProposalsRegistration = async () => {
        const { contract, isOwner } = this.state
    
        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut terminer l'enregistrement des propositions.")
                return
            }

            await contract.methods.endProposalsRegistration().send({ from: this.state.userAddress })
        } catch (error) {
            console.error("Erreur lors de la clôture de l'enregistrement des propositions :", error)
        }
    }

    registerProposal = async (newProposalDescription) => {
        const { contract, register} = this.state

        try {
            if (!register) {
                alert("Seul les votants inscrits peuvent enregistrer des propositions.")
                return
            }

            if (!this.state.proposals.includes(newProposalDescription)) {
                await contract.methods.registerProposal(newProposalDescription).send({ from: this.state.userAddress })
            } else {
                alert("La proposition est déjà enregistré.")
            }
        } catch (error) {
            console.error("Erreur lors de l'enregistrement de la proposition :", error)
        }
    }

    startVotingSession = async () => {
        const { contract, isOwner } = this.state

        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut commencer la session de vote.")
                return
            }

            await contract.methods.startVotingSession().send({ from: this.state.userAddress })
        } catch (error) {
            console.error("Erreur lors du démarrage de la session de vote :", error)
        }
    }

    endVotingSession = async () => {
        const { contract, isOwner } = this.state
    
        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut terminer la session de vote.")
                return
            }

            await contract.methods.endVotingSession().send({ from: this.state.userAddress })
        } catch (error) {
            console.error("Erreur lors de la fermeture de la session de vote :", error)
        }
    }

    registerVote = async (proposalId) => {
        const { contract } = this.state
    
        try {
            const contractStructVoters = await contract.methods.voters(this.state.userAddress).call();

            if (!contractStructVoters.isRegistered) {
                alert("Seul les votants inscrits peuvent voter.")
                return
            }
            if (contractStructVoters.hasVoted) {
                alert("Vous avez déjà voté.")
                return
            } else {
                const newProposalId = parseInt(proposalId, 10)
        
                await contract.methods.vote(newProposalId).send({ from: this.state.userAddress })
            }
        } catch (error) {
            console.error("Erreur lors de l'enregistrement du vote :", error)
        }
    }

    handleTallyVotes = async () => {
        const { contract, isOwner } = this.state
    
        try {   
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut comptabiliser les votes.")
                return
            }

            await contract.methods.tallyVotes().send({ from: this.state.userAddress })
        } catch (error) {
            console.error("Erreur lors de la comptabilisation des votes :", error)
        }
    }

    handleGetWinner = async () => {
        const { contract, isOwner } = this.state

        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut afficcher le gagant.")
                return
            }

            const winner = await contract.methods.getWinner().call({ from: this.state.userAddress })
            
            this.setState({ winningProposalId: winner })
        } catch (error) {
            console.error("Erreur lors de l'obtention du gagnant :", error)
        }
    }

    handleReset = async () => {
        const { contract, isOwner } = this.state
    
        try {
            if (!isOwner) {
                alert("Seul le propriétaire du contrat peut reset le contrat pour un nouveau vote.")
                return
            }

            await contract.methods.resetContract().send({ from: this.state.userAddress })
    
            this.setState({workflowStatus: -1, proposals: [], winningProposalId: null})         
        } catch (error) {
            console.error("Erreur lors de la réinitialisation du vote :", error)
        }
      }

    render() {
        const {
            userAddress,
            workflowStatus,
            winningProposalId,
            proposals,
            voters,
            votes,
            isOwner,
            register,
            voterHasVoted,
            contractOwner
        } = this.state

        return (
            <div className="container">
                <div className='row'>
                    <p className='col bg-danger'>Compte propriètaire : {contractOwner}</p>
                    <p className='col bg-success'>Compte connecté : {userAddress}</p>
                    <p className='col bg-warning'>Statut du workflow : {workflowStatus}</p>
                </div>
                <div className='row'>
                    <div className="col form-check">
                        <input className="form-check-input" type="radio" name="proprio" id="proprio" readOnly checked={isOwner} />
                        <label className="form-check-label form-check-inline" htmlFor="proprio">Propriétaire</label>
                    </div>
                    <div className="col form-check">
                        <input className="form-check-input" type="radio" name="save" id="save" readOnly checked={register} />
                        <label className="form-check-label form-check-inline" htmlFor="save">Enregistré</label>
                    </div>
                    <div className="col form-check">
                        <input className="form-check-input" type="radio" name="vote" id="vote" readOnly checked={voterHasVoted} />
                        <label className="form-check-label form-check-inline" htmlFor="vote">À voté</label>
                    </div>
                </div>
                <div className='row'>
                    <div className='col bg-secondary'> 
                        <p>Comptes enregistrés :</p>
                        <ul className="list-group">
                        {voters.map((voter, index) => (
                            <li className="list-group-item" key={index}>{voter}</li>
                        ))}
                        </ul>
                    </div>
                    <div className='col bg-secondary'>
                        <p>Propositions enregistés :</p>
                        <ul className="list-group">
                            {proposals.map((proposal, index) => (
                            <li className="list-group-item" key={index}>{proposal}</li>
                            ))}
                        </ul>
                    </div>
                </div>

                {workflowStatus === "0" && (
                    <div className="text-center">
                        <VoterRegistration
                            onRegisterVoter={this.handleRegisterVoter}
                            onStartPropals={this.startProposalsRegistration}
                            registeredVoters={voters}
                        />
                    </div>
                )}
              
                {workflowStatus === "1" && (
                    <div className="text-center">
                        <Proposal
                            onEndPropals={this.endProposalsRegistration}
                            registeredProposals={proposals}
                            registerProposal={this.registerProposal}
                        />
                    </div>
                )}
    
                {workflowStatus === "2" && (
                    <div className="text-center">
                        <button className='btn btn-warning' onClick={this.startVotingSession}>Commencer l'enregistrement des votes</button>
                    </div>
                )}
    
                {workflowStatus === "3" && (
                    <div className="text-center">
                        <VotingSession
                            registeredProposals={proposals}
                            registeredVotes={votes}
                            registerVote={this.registerVote}
                            onEndSession={this.endVotingSession}
                        />
                    </div>
                )}
        
                {workflowStatus === "4" && (
                    <div className="text-center">
                        <button className='btn btn-warning' onClick={this.handleTallyVotes}>Comptabilisation des votes</button>
                    </div>
                )}
    
                {workflowStatus === "5" && winningProposalId === null && (
                    <div className="text-center">
                        <button className='btn btn-warning' onClick={this.handleGetWinner}>Obtenir le gagnant</button>
                    </div>
                )}
        
                {winningProposalId !== null && (
                    <div className="text-center">
                        <Results
                            winner={proposals[winningProposalId]}
                            onReset={this.handleReset}
                        />
                    </div>
                )}
            </div>
        )
    }
}
    
export default App